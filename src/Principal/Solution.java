package Principal;

import Principal.Entity.Clients;
import Principal.Entity.Edges;
import Principal.Entity.Truck;
import Principal.Graph;
import Principal.Methods.VRP;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
/**
 * Classe Solution
 * Représente une solution
 */
public class Solution {
    private List<Edges> roads;
    private double distance = 0;
    private int nbTrucks = 0;

    private  int nbGraph = 1;
    /**
     * Constructeur de la classe Solution
     * @param trucks
     */
    private void createEdges(Truck[] trucks){
        this.roads = new ArrayList<>();
        int truckNumber=0;
        for(Truck truck : trucks){
            if(truck.clients.size()>2){
                for(int i=0;i<truck.clients.size()-1;i++){
                this.distance += VRP.distanceCalc(truck.clients.get(i),truck.clients.get(i+1));
                Edges edge = new Edges(truck.clients.get(i).getIdName() + truck.clients.get(i+1).getIdName(),
                        truck.clients.get(i).getIdName(),truck.clients.get(i+1).getIdName(),truckNumber);
                this.roads.add(edge);
                }
                this.nbTrucks++;
            }
            truckNumber++;
        }
    }

    /**
     * Calcul la distance parcourue par les camions
     * @param trucks
     * @return
     */
    public double calculDistance(Truck[] trucks){
        double distance = 0;
        for(Truck truck : trucks){
            if(truck.clients.size()>2) {
                for (int i = 0; i < truck.clients.size() - 1; i++) {
                    distance += VRP.distanceCalc(truck.clients.get(i), truck.clients.get(i + 1));
                }
            }
        }
        return distance;
    }
    /**
     * Calcul le nombre de camions utilisés
     * @param trucks
     * @return
     */
    public int calculNbTrucks(Truck[] trucks){
        int nbTrucks = 0;
        for(Truck truck : trucks){
            if(truck.clients.size()>2){
                nbTrucks++;
            }
        }
        return nbTrucks;
    }
    /**
     * Affiche la solution
     * @param trucks
     * @param clients
     * @param depot
     * @param maxQuantity
     */
    public void displaySoluce(Truck[]trucks, LinkedList<Clients> clients, Clients depot, int maxQuantity){
        this.distance = 0;
        this.nbTrucks = 0;
        this.createEdges(trucks);
        System.out.println("Nombre de véhicules minimum requis : "+ VRP.minTruck(maxQuantity, clients));
        System.out.println("Nombre de véhicules : "+ this.nbTrucks);
        System.out.println("Distance : " + this.distance);
        Graph graph = new Graph();
        graph.displayGraph(this.roads,clients,depot,this.nbGraph);
    }

    /**
     * Affiche les arêtes
     */
    private void displayEdges(){
        for(Edges edges: this.roads){
            edges.displayEdges();
        }
    }
    /**
     * Retourne le nombre de camions utilisés
     * @return
     */
    public int getNbTrucks(){
        return this.nbTrucks;
    }
}

package Principal;

import Principal.Entity.Clients;
import Principal.Entity.Edges;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.* ;

import java.util.LinkedList;
import java.util.List;
/**
 * Classe Graph
 * Représente le graphique de la solution
 */
public class Graph {
    /**
     * Affiche le graphique de la solution
     * @param edges
     * @param clients
     * @param depot
     * @param nbGraph
     */
    public void displayGraph(List<Edges> edges, LinkedList<Clients> clients, Clients depot, int nbGraph){
        org.graphstream.graph.Graph graph = new SingleGraph("Principal.Solution");
        graph.setAttribute("ui.stylesheet", "node { text-size: 20px; } edge { text-size: 20px; }");
        graph.setAttribute("ui.quality");
        graph.setAttribute("ui.antialias");
        graph.setStrict(false);
        Node depotn = graph.addNode(depot.getIdName());
        depotn.setAttribute("xy",depot.getCoord().getX(),depot.getCoord().getY());
        depotn.setAttribute("ui.style", "fill-color: rgb(255, 0, 0);");
        for(Clients client : clients){
            Node n = graph.addNode(client.getIdName());
            n.setAttribute("xy",client.getCoord().getX(),client.getCoord().getY());
        }

        for(Edges edge : edges){
            graph.addEdge(edge.name,edge.start,edge.end);
            if(graph.getEdge(edge.name) != null) graph.getEdge(edge.name)
                    .setAttribute("ui.style", "fill-color: rgb(" + (edge.indexTruck * 70) % 256 + ", "
                            + (edge.indexTruck * 10) % 256 + ", " + (255 - (edge.indexTruck * 20) % 256) + ");");
        }
        graph.display(false);
   }
}

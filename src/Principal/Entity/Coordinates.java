package Principal.Entity;

/**
 * Classe Coordinates
 * Représente les coordonnées d'un client
 */
public class Coordinates {
    private int x;
    private int y;

    /**
     * Constructor
     * @param x
     * @param y
     */
    public Coordinates(int x, int y){
        this.x=x;
        this.y=y;
    }
    /**
     * Affiche les coordonnées
     */
    public void affCoord(){
        System.out.println("x : "+x);
        System.out.println("y : "+y);
    }
    public int getX(){
        return this.x;
    }
    public void setX(int x){
        this.x = x;
    }
    public int getY(){
        return this.y;
    }
    public void setY(int y){
        this.y = y;
    }
}

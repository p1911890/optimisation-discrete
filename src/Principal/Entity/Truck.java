package Principal.Entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe Truck
 * Représente les camions
 */
public class Truck {
    public List<Clients> clients;
    private int capacity;
    public int time = 0;
    /**
     * Constructor
     * @param capacity
     */
    public Truck(int capacity){
        clients = new ArrayList<>();
        this.capacity = capacity;
    }
    /**
     * Clone les camions
     */
    public Truck clone() {
        Truck cloned = new Truck(this.capacity);
        cloned.clients.addAll(this.clients);
        cloned.time = this.time;
        return cloned;
    }

    /**
     * Vériie si deux camions sont égaux
     * @param o
     */
    @Override
    public boolean equals(Object o){
        Truck t = (Truck) o;
        if(t.clients.size()!=this.clients.size())return false;
        for(int i=0;i<this.clients.size();i++){
            if(!t.clients.get(i).getIdName().equals(this.clients.get(i).getIdName())){
                return false;
            }
        }
        return true;
    }

    public int getCapacity(){ return capacity;}

    public void setCapacity(int capacity){this.capacity = capacity;}

    /**
     * Affiche les clients
     */
    public void displayClients(){
        System.out.print("[");
        for (Clients client : clients){
            System.out.print(client.getIdName()+",");
        }
        System.out.print("]");
        System.out.println("");
    }

}

package Principal.Entity;

import Principal.Entity.Clients;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

/**
 * Classe Files
 * Représente les fichiers
 */
public class Files {

    private String pathToFile;

    public LinkedList<Clients> clientsTab;
    public Clients depot;
    public int maxQuantity;

    public int nbClients;

    /**
     * Constructor
     * @param pathToFile
     */
    public Files(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    /**
     * Affiche les informations du fichier
     */
    public void initClients(){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(this.pathToFile));
            String line;
            for(int i=0;i<5;i++){
                line = reader.readLine();
            }
            line = reader.readLine();
            this.nbClients = Integer.valueOf(line.split(" ")[1]);
            this.clientsTab = new LinkedList<>();
            line = reader.readLine();
            maxQuantity = Integer.valueOf(line.split(" ")[1]);
            line = reader.readLine();
            line = reader.readLine();
            line = reader.readLine();
            String vars[] = line.split(" ");
            depot = new Clients(vars[0],Integer.valueOf(vars[1]),Integer.valueOf(vars[2])
                    ,Integer.valueOf(vars[3]),Integer.valueOf(vars[4]),0,0);
            line = reader.readLine();
            line = reader.readLine();
            int i=0;
            while ((line = reader.readLine()) != null) {
                vars = line.split(" ");
                this.clientsTab.add(new Clients(vars[0],Integer.valueOf(vars[1]),Integer.valueOf(vars[2])
                        ,Integer.valueOf(vars[3]),Integer.valueOf(vars[4]),Integer.valueOf(vars[5]),Integer.valueOf(vars[6])));
                i++;
            }
            System.out.println("Les classes ont bien été initialisées à partir du fichier : "+ pathToFile);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

package Principal.Entity;

/**
 * Classe Edges
 * Représente les arcs du graphe
 */
public class Edges {
    public String name;
    public String start;

    public String end;

    public int indexTruck;

    /**
     * Constructor
     * @param name
     * @param start
     * @param end
     * @param indexTruck
     */
    public Edges(String name, String start, String end, int indexTruck){
        this.name = name;
        this.start = start;
        this.end = end;
        this.indexTruck= indexTruck;
    }

    /**
     * Affiche les arcs
     */
    public void displayEdges(){
        System.out.println("Name : "+this.name);
        System.out.println("Start : "+this.start);
        System.out.println("End : "+this.end);
        System.out.println("indexTruck : "+this.indexTruck);
    }
}

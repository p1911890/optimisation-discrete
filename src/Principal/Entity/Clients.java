package Principal.Entity;

import Principal.Entity.Coordinates;

/**
 * Classe Clients
 * Représente un client
 */
public class Clients {
    //idName x y readyTime dueTime
    private String idName;
    private Coordinates coord;
    private int readyTime;
    private int dueTime;
    private int  demand;
    private int service;

    /**
     * Constructor
     * @param idName
     * @param x
     * @param y
     * @param readyTime
     * @param dueTime
     * @param demand
     * @param service
     */
    public Clients(String idName, int x,int y, int readyTime, int dueTime, int demand,int service){
        this.idName = idName;
        this.coord = new Coordinates(x,y);
        this.readyTime = readyTime;
        this.dueTime = dueTime;
        this.demand = demand;
        this.service = service;
    }

    /**
     * Constructor
     */
    public Clients(){

    }

    /**
     * Affiche les informations du client
     */
    public void affClient(){
        System.out.println("idName : "+this.idName);
        this.coord.affCoord();
        System.out.println("readyTime : "+ this.readyTime);
        System.out.println("dueTime : "+this.dueTime);
        System.out.println("demand : "+this.demand);
        System.out.println("service : " +this.service);
    }

    public String getIdName() {
        return idName;
    }

    public Coordinates getCoord() {
        return coord;
    }

    public int getReadyTime() {
        return readyTime;
    }

    public int getDueTime() {
        return dueTime;
    }

    public int getDemand() {
        return demand;
    }

    public int getService() {
        return service;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public void setCoord(Coordinates coord) {
        this.coord = coord;
    }

    public void setIntCoord(int x, int y){
        this.coord.setX(x);
        this.coord.setY(y);
    }

    public void setReadyTime(int readyTime) {
        this.readyTime = readyTime;
    }

    public void setDueTime(int dueTime) {
        this.dueTime = dueTime;
    }

    public void setDemand(int demand) {
        this.demand = demand;
    }

    public void setService(int service) {
        this.service = service;
    }
}

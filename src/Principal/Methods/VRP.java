package Principal.Methods;

import Principal.Entity.Clients;

import java.util.*;
import Principal.Entity.*;
import Principal.*;
/**
 * Classe VRP
 * Représente le problème du voyageur de commerce
 */
public class VRP {

    /**
     * Calcule la quantité minimale de camions
     * @param maxQuantity
     * @param clients
     * @return
     */
    public static double minTruck(int maxQuantity, LinkedList<Clients> clients){
        int demandTotal=0;
        for (Clients client: clients){
            demandTotal+=client.getDemand();
        }
        return Math.ceil((double) demandTotal/maxQuantity);
    }

    /**
     * Donne une solution aléatoire
     * @param trucks
     * @param clients
     * @param depot
     * @return
     */
    public static Truck[] randomSoluce(Truck[] trucks, LinkedList<Clients> clients, Clients depot) {
        LinkedList<Clients> clientsTemp = new LinkedList<>();
        clientsTemp.addAll(clients);
        // Pour chaque camion
        for (Truck truck : trucks) {
            // Créer une liste des clients non visités pour le camion en cours
            List<Clients> clientsListNotVisitable = new ArrayList<Clients>();
            int clientSize = clients.size();
            // Tant que le camion a de la capacité et qu'il y a des clients non visités
            while (truck.getCapacity() > 0 && clientsListNotVisitable.size() < clientSize) {
                // Choisir un client aléatoire à visiter
                Random rand = new Random();
                int random = rand.nextInt(clients.size());
                Clients clientChosen = clients.get(random);
                // Si le camion a suffisamment de capacité pour le client choisi
                if (truck.getCapacity() > clientChosen.getDemand()) {
                    // Calculer la distance entre le dernier client visité par le camion et le client choisi
                    double distance = VRP.distanceCalc(truck.clients.get(truck.clients.size() - 1), clientChosen);
                    // Si le temps nécessaire pour se rendre au client choisi est inférieur à l'heure de fin de visite
                    double distanceDepot = VRP.distanceCalc(clientChosen, depot);
                    if (distance + truck.time <  clientChosen.getDueTime()
                            && Math.max(distance + truck.time, clientChosen.getReadyTime()) + clientChosen.getService() + distanceDepot <= depot.getDueTime()) {
                        // Mettre à jour le temps de départ du camion
                        truck.time += distance;
                        // Si le temps de départ est inférieur à l'heure de début de visite
                        if (truck.time < clientChosen.getReadyTime()) {
                            truck.time = clientChosen.getReadyTime();
                        }
                        // Mettre à jour le temps de départ avec le temps de service du client choisi
                        truck.time += clientChosen.getService();
                        // Mettre à jour la capacité restante du camion
                        truck.setCapacity(truck.getCapacity() - clientChosen.getDemand());
                        // Ajouter le client choisi à la liste des clients visités par le camion
                        truck.clients.add(clientChosen);
                        clients.remove(clientChosen);
                    }
                }
                // Ajouter le client choisi à la liste des clients non visitables pour le camion en cours
                clientsListNotVisitable.add(clientChosen);
            }
            // Le camion retourne au dépot pour terminer sa tournée
            truck.clients.add(depot);
        }
        return trucks;
    }

    /**
     * Rafraichit la liste des camions
     * @param trucks
     */
    public static Truck[] refreshTrucks(Truck[] trucks) {
        return Arrays.stream(trucks)
                .filter(truck -> truck.clients.size() > 2)
                .toArray(Truck[]::new);
    }

    /**
     * Calcule la distance entre deux clients
     * @param client1
     * @param client2
     * @return
     */
    public static double distanceCalc(Clients client1, Clients client2){
        double distanceX = Math.pow((client2.getCoord().getX() - client1.getCoord().getX()),2);
        double distanceY = Math.pow((client2.getCoord().getY() - client1.getCoord().getY()),2);
        return Math.sqrt(distanceX+distanceY);
    }


}

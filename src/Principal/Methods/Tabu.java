package Principal.Methods;

import Principal.Neighbor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;import Principal.Entity.*;
import Principal.*;

/**
 * Classe Tabu
 * Représente l'algorithme tabou
 */
public class Tabu {
    /**
     * Algorithme tabou
     * @param trucks
     * @param listTabuMax
     * @param nbVoisins
     * @param iterations
     * @return
     */
    public Truck[] tabuMethod(Truck[] trucks, int listTabuMax, int nbVoisins, int iterations) {
        Truck[] truckMin = Arrays.stream(trucks).map(Truck::clone).toArray(Truck[]::new);
        Truck[] truckFinal = Arrays.stream(trucks).map(Truck::clone).toArray(Truck[]::new);
        Solution soluce = new Solution();
        double soluceMin = soluce.calculDistance(truckMin);
        List<String> listTabu = new ArrayList<>();
        for (int i = 0; i < iterations; i++) {
            Truck[] tempTrucks = Arrays.copyOf(truckMin, truckMin.length);
            Truck[] finalTruckMin = truckMin;
            String tabuString = "";
            List<HashMap<String, Truck[]>> neighborsMap = IntStream.range(0, nbVoisins)
                    .mapToObj(k -> {
                        Neighbor neighbor = new Neighbor();
                        int nbTrucks = soluce.calculNbTrucks(tempTrucks);
                        return neighbor.getRandomNeighbor(tempTrucks, nbTrucks,k);
                    }).filter(Objects::nonNull)
                    .filter(neighbor -> !listTabu.contains(neighbor.keySet().iterator().next()))
                    //.filter(neighbor->!(soluce.calculDistance(finalTruckMin)==soluce.calculDistance(neighbor.get(neighbor.keySet().iterator().next()))))
                    .collect(Collectors.toList());

            List<Truck[]> neighborsList = new ArrayList<>();

            List<String> listeDeCles = new ArrayList<>();
            // Parcourir chaque HashMap dans la liste
            for (HashMap<String, Truck[]> neighborMap : neighborsMap) {
                // Récupérer toutes les valeurs (camions) dans la HashMap
                Collection<Truck[]> truckArrays = neighborMap.values();
                Set<String> keys = neighborMap.keySet();

                // Ajouter les clés dans la liste distincte
                listeDeCles.addAll(keys);
                // Parcourir chaque tableau de camions dans la collection
                for (Truck[] truckArray : truckArrays) {
                    // Ajouter le camion dans la liste allTrucks
                    neighborsList.add(truckArray);
                }
            }
            //int l = (int) listeDeCles.stream().filter(listTabu::contains).count();
            //System.out.println(l);
            /*for(String clef: listeDeCles){
                String tempclef = clef.substring(0,3);
                if(tempclef.contains("n3")|| tempclef.contains("n4")|| tempclef.contains("n5") || tempclef.contains("n6")){
                    System.out.println(clef);
                }
            }*/
            if(neighborsList.size()>0) {
                Optional<AbstractMap.SimpleEntry<Integer, Truck[]>> bestNeighborWithIndex = IntStream.range(0, neighborsList.size())
                        .mapToObj(index -> new AbstractMap.SimpleEntry<>(index, neighborsList.get(index)))
                        .min(Comparator.comparingDouble(entry -> soluce.calculDistance(entry.getValue())));
                int bestIndex = bestNeighborWithIndex.get().getKey();
                Truck[] bestNeighbor = bestNeighborWithIndex.get().getValue();
                bestNeighbor = VRP.refreshTrucks(bestNeighbor);
                if (soluceMin <= soluce.calculDistance(bestNeighbor)) {
                    listTabu.add(listeDeCles.get(bestIndex));
                }
                truckMin = bestNeighbor.clone();
                soluceMin = soluce.calculDistance(truckMin);
                if(soluceMin<soluce.calculDistance(truckFinal)){
                    truckFinal = Arrays.stream(truckMin).map(Truck::clone).toArray(Truck[]::new);
                }
                //System.out.println(soluceMin);
                if (listTabu.size() > listTabuMax) {
                    listTabu.remove(0);
                }
                /*System.out.print("[");
                for (String element : listTabu) {
                    System.out.print(element + ";");
                }
                System.out.print("]");
                System.out.println("");*/
            }
        }
        return truckFinal;
    }
}

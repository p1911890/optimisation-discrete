package Principal.Methods;

import Principal.Neighbor;
import Principal.Entity.*;
import Principal.*;
import java.util.Arrays;
import java.util.Random;
/**
 * Classe SimulatedAnnealing
 * Représente l'algorithme de recuit simulé
 */
public class SimulatedAnnealing {
    /**
     * Calcul de la température initiale
     */
    public double getTemperatureInitial(Truck[] trucks){
        trucks = VRP.refreshTrucks(trucks);
        Solution soluce = new Solution();
        double deltaFMin = 0;
        double lnP = Math.log(0.8);
        double distanceInitial = soluce.calculDistance(trucks);
        for (int i=0; i < 700; i++){
            Neighbor neighbor = new Neighbor();
            Truck[] neighbors = neighbor.getRandomNeighbor(trucks, trucks.length);
            if (neighbors != null){
                double distanceNeighbor = soluce.calculDistance(neighbors);
                double deltaFTemp = distanceInitial - distanceNeighbor;
                if(deltaFTemp > deltaFMin){
                    deltaFMin = deltaFTemp;
                }
            }
            //System.out.println("delta F : " + deltaFMin);
        }
        double temperature = -deltaFMin/lnP;
        return temperature;
    }

    /*public int maxiterationCalc (double t, double mu){
        int n = 0;
        double logP = Math.log(0.8);
        double logPn = Math.log(0.001);
        double invLogMu = 1/Math.log(mu);
        double log = Math.log(logP/logPn);
        n = (int) Math.ceil(log*invLogMu);
        return n;
    }*/

    /*public int temperatureCalc(int distance1, int distance2, double p){
        double lnP = Math.log(p);
        int deltaF = distance1 - distance2;
        int t = (int) Math.ceil(-deltaF/lnP);
        return t;
    }*/

    /**
     * Recuit simulé
     * @param trucks
     * @param coolingTemperature
     * @param finalTemp
     * @param maxIterations
     */
    public Truck[] simulatedAnnealing(Truck[] trucks, double coolingTemperature, double finalTemp, int maxIterations){
        Random rand = new Random();
        Neighbor neighbor = new Neighbor();
        Solution soluce = new Solution();
        double temperatureTemp = this.getTemperatureInitial(trucks);
        Truck[] truckMin = Arrays.stream(trucks).map(Truck::clone).toArray(Truck[]::new);
        Truck[] truckFinal = Arrays.stream(trucks).map(Truck::clone).toArray(Truck[]::new);
        while(temperatureTemp > finalTemp){
            for(int i=0; i <maxIterations; i++){
                Truck[] tempTrucks = Arrays.stream(truckMin).map(Truck::clone).toArray(Truck[]::new);
                int nbTrucks = soluce.calculNbTrucks(tempTrucks);
                tempTrucks = neighbor.getRandomNeighbor(tempTrucks, nbTrucks);
                if(tempTrucks != null) {
                    if(soluce.calculDistance(tempTrucks) != soluce.calculDistance(truckMin)){
                        // calcul la proba d'acceptation de prendre la solution
                        double distanceMin = soluce.calculDistance(truckMin);
                        double distanceTemp = soluce.calculDistance(tempTrucks);
                        double probability = this.acceptanceProba(distanceMin, distanceTemp, temperatureTemp);
                        // si ton p est <= à la proba alors :
                        if (rand.nextDouble() <= probability) {
                            // On fait le changement
                            // ca devient notre nouvelle solution
                            truckMin = tempTrucks;
                            truckMin = VRP.refreshTrucks(truckMin);
                            if(soluce.calculDistance(truckFinal) > soluce.calculDistance(truckMin)){
                                truckFinal = Arrays.stream(truckMin).map(Truck::clone).toArray(Truck[]::new);
                            }
                        }
                    }
                }
            }
            // On fait descendre la température
            temperatureTemp *= coolingTemperature;
        }
        return truckFinal;
    }

    /**
     * Calcul de la probabilité d'acceptation
     */
    public double acceptanceProba(double distanceMin,double distanceTemp, double temperature){
        if(distanceTemp < distanceMin) {
            return 1.0;
        }
        return Math.exp((distanceMin - distanceTemp) / temperature);
    }

}

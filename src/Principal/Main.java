package Principal;

import Principal.Entity.*;
import Principal.Methods.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe Main
 * Classe principale du projet
 * Permet de lancer les différentes méthodes de résolution
 * Affiche les résultats
 */
public class Main {

    public static void main(String[] args) {
        Files file = new Files("Data/data1201.vrp");
        file.initClients();

        double total = 0;
        double min = Double.MAX_VALUE;
        double tempsTotal = 0;
        double tempsPire = 0;

        // Valeur à modifier pour changer le nombre de fois qu'un algo est lancé
        int wmax = 20;

        Truck[] minTruck = new Truck[60];
        Solution soluce = new Solution();
        for(int w = 0;w<wmax;w++){

            LinkedList<Clients> clients = (LinkedList<Clients>) file.clientsTab.clone();
            Clients depot = file.depot;
            Truck[] trucks = new Truck[60];
            for(int i=0; i< trucks.length;i++){
                trucks[i] = new Truck(file.maxQuantity);
                trucks[i].clients.add(depot);
            }

            VRP.randomSoluce(trucks,clients,depot);
            trucks = VRP.refreshTrucks(trucks);
            long startTime = System.nanoTime();

            // Lignes à décommenter / commenter pour choisir la méthode que l'on souhaite lancer

            //Tabou : taille de la liste, nombre de voisins, nombre d'itérations
            /*Principal.Methods.Tabu tabu = new Principal.Methods.Tabu();
            trucks = tabu.tabuMethod(trucks, 50,700, 1000);*/

            //Recuit rapide : température de refroidissement, température finale, itérations max
            SimulatedAnnealing simulatedAnnealing = new SimulatedAnnealing();
            trucks = simulatedAnnealing.simulatedAnnealing(trucks, 0.9, 0.01, 5000);

            //Recuit maximisé : température de refroidissement, température finale, itérations max
            /*SimulatedAnnealing simulatedAnnealing = new SimulatedAnnealing();
            trucks = simulatedAnnealing.simulatedAnnealing(trucks, 0.99, 0.001, 10000);*/

            trucks = Principal.Methods.VRP.refreshTrucks(trucks);

            double value = soluce.calculDistance(trucks);
            total += value;
            if(Math.round(value) < Math.round(min)){
                min = value;
                minTruck = Arrays.stream(trucks).map(Truck::clone).toArray(Truck[]::new);
            }
            long endTime = System.nanoTime();
            long duration = (endTime - startTime); // durée en nanosecondes
            double seconds = (double)duration / 1_000_000_000.0; // durée en secondes
            tempsTotal += seconds;
            if(tempsPire<seconds){
                tempsPire = seconds;
            }
            System.out.println("itération : " + w);
        }
        soluce.displaySoluce(minTruck,file.clientsTab,file.depot, file.maxQuantity);
        System.out.println("Meilleure fitness : " + min);
        System.out.println("Moyenne de fitness : " + total/wmax);
        System.out.println("Moyenne de temps : " + tempsTotal/wmax);
        System.out.println("Pire temps : " + tempsPire);
    }
}
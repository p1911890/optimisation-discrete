package Principal;

import java.util.LinkedList;
import Principal.Entity.*;
import Principal.Methods.*;
/**
 * Classe Operator
 * Représente un opérateur
 */
public abstract class Operator {
    /**
     * Applique l'opérateur
     * @param trucks
     * @param index1
     * @param index2
     */
    public abstract Truck[] apply(Truck[] trucks, int index1, int index2);

    protected String indexes;
    /**
     * Vérifie si la solution est possible
     * @param clients
     */
    public boolean checkPossible(LinkedList<Clients> clients){
        int quantity = 200;
        double time = 0;
        for(int i=0;i<clients.size()-1;i++){
            time += VRP.distanceCalc(clients.get(i),clients.get(i+1));
            time = Math.max(time, clients.get(i+1).getReadyTime());
            if(time>clients.get(i+1).getDueTime()){
                return false;
            }
            time += clients.get(i+1).getService();
            quantity -= clients.get(i).getDemand();
            if(quantity<0){
                return false;
            }
        }
        return true;
    }

    public String getIndexes(){
        return this.indexes;
    }
}

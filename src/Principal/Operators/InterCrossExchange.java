package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.LinkedList;
import java.util.Random;
/**
 * Classe InterCrossExchange
 * Représente l'opérateur InterCrossExchange
 */
public class InterCrossExchange extends Operator {
    /**
     * Applique l'opérateur InterCrossExchange
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck1 = trucks[index1];
        Truck truck2 = trucks[index2];
        Truck[] result = interCrossExchange(truck1,truck2);
        if(result!=null) {
            trucks[index1] = result[0];
            trucks[index2] = result[1];
        }else{
            return null;
        }
        return trucks;
    }

    /**
     * Applique l'opérateur InterCrossExchange
     * @param truck1
     * @param truck2
     */
    public Truck[] interCrossExchange(Truck truck1, Truck truck2){
        Random rand = new Random();
        int index1Start = rand.nextInt(truck1.clients.size() - 1)+1;
        int index1End = rand.nextInt(truck1.clients.size() - index1Start) + index1Start ;
        int index2Start = rand.nextInt(truck2.clients.size() - 1)+1;
        int index2End = rand.nextInt(truck2.clients.size() - index2Start) + index2Start ;
        LinkedList<Clients> clientsCheck1 = new LinkedList<>(truck1.clients);
        LinkedList<Clients> clientsCheck2 = new LinkedList<>(truck2.clients);
        LinkedList<Clients> listClientsRm1 = new LinkedList<>();
        LinkedList<Clients> listClientsRm2 = new LinkedList<>();
        Clients client1 = new Clients();
        for(int i=index1Start;i<index1End;i++){
            client1 = truck1.clients.get(i);
            listClientsRm1.add(client1);
            clientsCheck1.remove(index1Start);
        }
        Clients client2= new Clients();
        for(int i=index2Start;i<index2End;i++){
            client2 = truck2.clients.get(i);
            listClientsRm2.add(client2);
            clientsCheck2.remove(index2Start);
        }
        clientsCheck1.addAll(index1Start, listClientsRm2);
        clientsCheck2.addAll(index2Start, listClientsRm1);
        if(checkPossible(clientsCheck1) && checkPossible(clientsCheck2)){
            truck1.clients = clientsCheck1;
            truck2.clients = clientsCheck2;
            Truck[] result = new Truck[2];
            result[0]=truck1;
            result[1]=truck2;
            this.indexes =  client1.getIdName() +"/"+client2.getIdName();
            return result;
        }
        return null;
    }
}

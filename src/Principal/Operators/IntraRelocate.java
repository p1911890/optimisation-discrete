package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.LinkedList;
import java.util.Random;
/**
 * Classe IntraRelocate
 * Représente l'opérateur IntraRelocate
 */
public class IntraRelocate extends Operator {
    /**
     * Applique l'opérateur IntraRelocate
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck = trucks[index1];
        Truck newTruck = intraRelocate(truck);
        if (newTruck != null) {
            trucks[index1] = newTruck;
        }else{
            return null;
        }
        return trucks;
    }

    /**
     * Applique l'opérateur IntraRelocate
     * @param truck
     */
    public Truck intraRelocate(Truck truck) {
        Random rand = new Random();
        int indexRemove = rand.nextInt(truck.clients.size() - 2)+1;
        //truck.displayClients();
        Clients tempClient = truck.clients.get(indexRemove);
        int indexReplace;
        do{ indexReplace = rand.nextInt(truck.clients.size() - 2)+1;}while(indexReplace==indexRemove);
        LinkedList<Clients> clientsCheck = new LinkedList<>(truck.clients);
        clientsCheck.remove(indexRemove);
        clientsCheck.add(indexReplace, tempClient);
        if (checkPossible(clientsCheck)) {
            truck.clients.remove(indexRemove);
            truck.clients.add(indexReplace, tempClient);
            this.indexes = clientsCheck.get(indexReplace).getIdName() +"/" + clientsCheck.get(indexRemove).getIdName();
            return truck;
        }
        return null;
    }
}
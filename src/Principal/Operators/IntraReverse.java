package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.Collections;
import java.util.LinkedList;
/**
 * Classe IntraReverse
 * Représente l'opérateur IntraReverse
 */
public class IntraReverse extends Operator {
    /**
     * Applique l'opérateur IntraReverse
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck = trucks[index1];
        Truck newTruck = intraReverse(truck);
        if (newTruck != null) {
            trucks[index1] = newTruck;
        }else{
            return null;
        }
        return trucks;
    }

    /**
     * Applique l'opérateur IntraReverse
     * @param truck
     */
    public Truck intraReverse(Truck truck){
        LinkedList<Clients> clientsCheck = new LinkedList<>(truck.clients);
        Collections.reverse(clientsCheck);
        if(checkPossible(clientsCheck)){
            Collections.reverse(truck.clients);
            this.indexes = "";
            return truck;
        }
        return null;
    }

}

package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.LinkedList;
import java.util.Random;
/**
 * Classe InterExchange
 * Représente l'opérateur InterExchange
 */
public class InterExchange extends Operator {
    /**
     * Applique l'opérateur InterExchange
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck1 = trucks[index1];
        Truck truck2 = trucks[index2];
        Truck[] result = interExchange(truck1,truck2);
        if(result!=null) {
            trucks[index1] = result[0];
            trucks[index2] = result[1];
        }else{
            return null;
        }
        return trucks;
    }
    /**
     * Applique l'opérateur InterExchange
     * @param truck1
     * @param truck2
     */
    public Truck[] interExchange(Truck truck1, Truck truck2){
        Random rand = new Random();
        int indexExchange1 = rand.nextInt(truck1.clients.size() - 2)+1;
        int indexExchange2 = rand.nextInt(truck2.clients.size() - 2)+1;
        int indexReplace1 = rand.nextInt(truck1.clients.size()-2)+1;
        int indexReplace2 = rand.nextInt(truck2.clients.size()-2)+1;
        LinkedList<Clients> clientsCheck1 = new LinkedList<>(truck1.clients);
        LinkedList<Clients> clientsCheck2 = new LinkedList<>(truck2.clients);
        Clients client1 = truck1.clients.get(indexExchange1);
        Clients client2 = truck2.clients.get(indexExchange2);
        clientsCheck1.remove(indexExchange1);
        clientsCheck2.remove(indexExchange2);
        clientsCheck1.add(indexReplace1, client2);
        clientsCheck2.add(indexReplace2, client1);
        if(checkPossible(clientsCheck1) && checkPossible(clientsCheck2)){
            truck1.clients = clientsCheck1;
            truck2.clients = clientsCheck2;
            Truck[] result = new Truck[2];
            result[0]=truck1;
            result[1]=truck2;
            this.indexes = client1.getIdName() +"/"+ client2.getIdName();
            return result;
        }
        return null;

    }

}

package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.LinkedList;
import java.util.Random;
/**
 * Classe Intra2Opt
 * Représente l'opérateur Intra2Opt
 */
public class Intra2Opt extends Operator {
    /**
     * Applique l'opérateur Intra2Opt
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck = trucks[index1];
        Truck newTruck = intra2Opt(truck);
        if (newTruck != null) {
            trucks[index1] = newTruck;
        }else{
            return null;
        }
        return trucks;
    }
    /**
     * Applique l'opérateur Intra2Opt
     * @param truck
     */
    public Truck intra2Opt(Truck truck){
        Random rand = new Random();
        int indexi = rand.nextInt(truck.clients.size()-2)+1;
        int indexj = rand.nextInt(truck.clients.size()-2)+1;
        while(Math.abs(indexj-indexi)<1 || indexj==indexi){indexj = rand.nextInt(truck.clients.size()-2)+1;}
        if(indexi>indexj){
            int tempIndex = indexj;
            indexj = indexi;
            indexi = tempIndex;
        }
        LinkedList<Clients> clientsCheck = new LinkedList<>(truck.clients);
        this.reverseListBetweenIndices(clientsCheck,indexi,indexj);
        if(checkPossible(clientsCheck)){
            truck.clients = clientsCheck;
            this.indexes = truck.clients.get(indexi).getIdName() + "/"+ truck.clients.get(indexj).getIdName();
            return truck;
        }
        return null;
    }
    /**
     * Inverse les éléments d'une liste entre deux indices
     * @param list
     * @param startIndex
     * @param endIndex
     */
    public void reverseListBetweenIndices(LinkedList<Clients> list, int startIndex, int endIndex) {
        while (startIndex < endIndex) {
            // Échange des éléments aux indices startIndex et endIndex
            Clients temp = list.get(startIndex);
            list.set(startIndex, list.get(endIndex));
            list.set(endIndex, temp);

            // Déplacement des indices
            startIndex++;
            endIndex--;
        }
    }
}

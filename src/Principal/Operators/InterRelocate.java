package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.LinkedList;
import java.util.Random;
/**
 * Classe InterCrossExchange
 * Représente l'opérateur InterRelocate
 */
public class InterRelocate extends Operator {
    /**
     * Applique l'opérateur InterRelocate
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck1 = trucks[index1];
        Truck truck2 = trucks[index2];
        Truck[] result = interRelocate(truck1,truck2);
        if(result!=null) {
            trucks[index1] = result[0];
            trucks[index2] = result[1];
        }else{
            return null;
        }
        return trucks;
    }

    /**
     * Applique l'opérateur InterRelocate
     * @param truck1
     * @param truck2
     */
    public Truck[] interRelocate(Truck truck1, Truck truck2){
        Random rand = new Random();
        int indexRemove = rand.nextInt(truck1.clients.size() - 2)+1;
        Clients tempClient = truck1.clients.get(indexRemove);
        int indexReplace = rand.nextInt(truck2.clients.size() - 2)+1;
        LinkedList<Clients> clientsCheck = new LinkedList<>(truck2.clients);
        clientsCheck.add(indexReplace, tempClient);
        if (checkPossible(clientsCheck)) {
            truck1.clients.remove(indexRemove);
            truck2.clients.add(indexReplace, tempClient);
            Truck[] result = new Truck[2];
            result[0]=truck1;
            result[1]=truck2;
            this.indexes = tempClient.getIdName() +"/"+ truck2.clients.get(indexReplace).getIdName();
            return result;
        }
        return null;
    }
}

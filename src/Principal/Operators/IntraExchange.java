package Principal.Operators;

import Principal.Entity.Clients;
import Principal.Entity.Truck;
import Principal.Operator;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;
/**
 * Classe IntraExchange
 * Représente l'opérateur IntraExchange
 */
public class IntraExchange extends Operator {
    /**
     * Applique l'opérateur IntraExchange
     * @param trucks
     * @param index1
     * @param index2
     */
    @Override
    public Truck[] apply(Truck[] trucks, int index1, int index2) {
        Truck truck = trucks[index1];
        Truck newTruck = intraExchange(truck);
        if (newTruck != null) {
            trucks[index1] = newTruck;
        }else{
            return null;
        }
        return trucks;
    }

    /**
     * Applique l'opérateur IntraExchange
     * @param truck
     */
    public Truck intraExchange(Truck truck){
        Random rand = new Random();
        int indexExchange1 = rand.nextInt(truck.clients.size() - 2)+1;
        int indexExchange2 = rand.nextInt(truck.clients.size() - 2)+1;
        do{ indexExchange2 = rand.nextInt(truck.clients.size() - 2)+1;}while(indexExchange1==indexExchange2 || Math.abs(indexExchange1-indexExchange2)>2);
        LinkedList<Clients> clientsCheck = new LinkedList<>(truck.clients);
        Collections.swap(clientsCheck, indexExchange1, indexExchange2);
        if(checkPossible(clientsCheck)){
            Collections.swap(truck.clients, indexExchange1, indexExchange2);
            this.indexes = truck.clients.get(indexExchange1).getIdName() + "/"+ truck.clients.get(indexExchange2).getIdName();
            return truck;
        }
        return null;
    }
}

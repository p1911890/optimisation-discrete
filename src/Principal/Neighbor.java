package Principal;


import java.util.Arrays;
import java.util.Random;
import java.util.function.Predicate;
import java.util.HashMap;
import Principal.Entity.*;
import Principal.Methods.*;
import Principal.Operators.*;
/**
 * Classe Neighbor
 * Représente un voisin
 */
public class Neighbor {

    private int maxQuantity;
    /**
     * Donne un voisinage aléatoire
     * @param trucks
     * @param size
     * @param kneighbor
     */
    public HashMap<String, Truck[]> getRandomNeighbor(Truck[] trucks, int size, int kneighbor) {
        HashMap<String, Truck[]> result = new HashMap<>();
        Random rand = new Random();
        int randNeighbor = kneighbor % 7;
        int borneInf = this.getBorneInf(randNeighbor);
        int index1 = getRandomTruckIndex(trucks, size, rand, truck -> truck.clients.size() > borneInf, -1);
        int index2 = 0;
        if(randNeighbor==0 || randNeighbor == 1 || randNeighbor==2){
            if(size>1){
                index2 = getRandomTruckIndex(trucks, size, rand, truck -> truck.clients.size() > borneInf, index1);
            }else{
                return null;
            }
        }


        if (index1 == -1 || index2 == -1) {
            return null;
        }

        Truck[] newTrucks = cloneTrucks(trucks);
        Operator operator = getOperator(randNeighbor);

        if (operator != null) {
            newTrucks = operator.apply(newTrucks, index1, index2);
            if (newTrucks == null) { // Vérification de la valeur de retour
                return null;
            }
        } else {
            return null;
        }
        result.put("n"+randNeighbor+" r"+index1+"/"+index2, newTrucks);
        return result;
    }
    /**
     * Donne une voisinage aléatoire
     * @param trucks
     * @param size
     */
    public Truck[] getRandomNeighbor(Truck[] trucks, int size) {
        HashMap<String, Truck[]> result = new HashMap<>();
        Random rand = new Random();
        int randNeighbor = rand.nextInt(7);
        int borneInf = this.getBorneInf(randNeighbor);
        int index1 = getRandomTruckIndex(trucks, size, rand, truck -> truck.clients.size() > borneInf, -1);
        int index2 = 0;
        if(randNeighbor==0 || randNeighbor == 1 || randNeighbor==2){
            if(size>1){
                index2 = getRandomTruckIndex(trucks, size, rand, truck -> truck.clients.size() > borneInf, index1);
            }else{
                return null;
            }
        }


        if (index1 == -1 || index2 == -1) {
            this.getRandomNeighbor(trucks,size);
            return null;
        }

        Truck[] newTrucks = cloneTrucks(trucks);
        Operator operator = getOperator(randNeighbor);

        if (operator != null) {
            newTrucks = operator.apply(newTrucks, index1, index2);
            if (newTrucks == null) { // Vérification de la valeur de retour
                this.getRandomNeighbor(trucks,size);
                return null;
            }
        } else {
            this.getRandomNeighbor(trucks,size);
            return null;
        }
        return newTrucks;
    }

    /**
     * Clone les camions
     * @param trucks
     */
    private Truck[] cloneTrucks(Truck[] trucks) {
        return Arrays.stream(trucks).map(Truck::clone).toArray(Truck[]::new);
    }
    /**
     * Donne la borne inférieure en fonction du voisinage
     * @param neighborRand
     */
    private int getBorneInf(int neighborRand){
        if(neighborRand==2 || neighborRand == 0 || neighborRand == 1)return 2;
        else if(neighborRand==6)return 4;
        else if(neighborRand==5)return 0;
        else return 3;
    }
    /**
     * Donne un index aléatoire d'un camion qui respecte le prédicat
     * @param trucks
     * @param size
     * @param rand
     * @param predicate
     * @param excludeIndex
     * @return
     */
    private int getRandomTruckIndex(Truck[] trucks, int size, Random rand, Predicate<Truck> predicate, int excludeIndex) {
        int index;
        do {
            index = rand.nextInt(size);
        } while (!predicate.test(trucks[index]) || index == excludeIndex);
        return index;
    }
    /**
     * Choisit un opérateur de voisinage
     * @param randNeighbor
     */
    private Operator getOperator(int randNeighbor) {
        return switch (randNeighbor) {
            case 0 -> new InterCrossExchange();
            case 1 -> new InterExchange();
            case 2 -> new InterRelocate();
            case 3 -> new IntraRelocate();
            case 4 -> new IntraExchange();
            case 5 -> new IntraReverse();
            case 6 -> new Intra2Opt();
            default -> null;
        };
    }


}
